<?php

/**
 * @file
 * Contains \Drupal\content_pager\src\Form\Pagingform.
 */
namespace Drupal\content_pager\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class Pagingform extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contentpager';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $contentTypes = \Drupal::service('entity.manager')
      ->getStorage('node_type')
      ->loadMultiple();
    // Set the id of the top-level form tag.
    $form['#id'] = 'contentpager';
    $form['paging_general'] = array(
      '#type' => 'fieldset',
      '#title' => t('General paging settings'),
      '#collapsible' => FALSE,
    );

    // Paging separator string.
    // @TODO will need an upgrade path.
    /*$form['paging_general']['paging_separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Page separator string'),
      '#size' => 20,
      '#maxlength' => 255,
      '#description' => t('Use an HTML tag that will render reasonably when
      paging is not enabled, such as %pagebreak or %hr.'),
      '#default_value' => \Drupal::config('content_pager.separator')
        ->get('content_pager.separator')
    );*/

    $form['paging_general']['paging_pager_count'] = array(
      '#type' => 'radios',
      '#title' => t('Number of Pagers on each page'),
      '#options' => array(
        'one' => t('One'),
        'two' => t('Two'),
      ),
      '#attributes' => array('class' => array('paging-pager')),
      '#default_value' => \Drupal::config('content_pager.pagerCount')
        ->get('content_pager.pagerCount')
    );

    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {

      $contentTypesList[$contentType->id()] = $contentType->label();
      $content_type = $contentType->id();
      $form[$content_type] = array(
        '#type' => 'fieldset',
        '#title' => $content_type,
      );

      $field_options = array('body');
      // Left column fieldset.
      $form[$content_type]['paging_config']['paging_left'] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#attributes' => array('class' => array('paging-left')),
      );

      // Paging toggle checkbox.
      $form[$content_type]['paging_config']['paging_left']['paging_enabled_' . $content_type] = array(
        '#type' => 'checkbox',
        '#title' => 'Enable paging',
        '#attributes' => array('class' => array('paging-enabled')),
        '#default_value' => \Drupal::config('content_pager.enabled_' . $content_type)
          ->get('content_pager.enabled_' . $content_type)
      );

      // Paging toggle checkbox.
      $form[$content_type]['paging_config']['paging_left']['paging_field_' . $content_type] = array(
        '#type' => 'radios',
        '#title' => 'Select field to use for page breaks',
        '#options' => $field_options,
        '#attributes' => array('class' => array('paging-enabled')),
        '#states' => array(
          'visible' => array(
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
          ),
        ),
        '#default_value' => \Drupal::config('content_pager.field_' . $content_type)
          ->get('content_pager.field_' . $content_type)
      );

      // Change "Read more" path when first page is greater than or equal to the teaser.
      $form[$content_type]['paging_config']['paging_left']['paging_read_more_enabled_' . $content_type] = array(
        '#type' => 'checkbox',
        '#title' => t('Link "Read more" to second page'),
        '#description' => t('When enabled, the "Read more" link for teasers will
        link to the second page of the content if the teaser is larger than the
        first page or if they are the same.'),
        '#states' => array(
          'visible' => array(
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
          ),
        ),
        '#default_value' => \Drupal::config('content_pager.readMore_' . $content_type)
          ->get('content_pager.readMore_' . $content_type)
      );

      // Set the browser's title to current page's name.
      $form[$content_type]['paging_config']['paging_left']['paging_name_title_' . $content_type] = array(
        '#type' => 'checkbox',
        '#title' => t('Change page title to name of current page'),
        '#description' => t("Change the node's and browser window's title into
        name of the current page."),
        '#states' => array(
          'visible' => array(
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
          ),
        ),
        '#default_value' => \Drupal::config('content_pager.changeTitle_' . $content_type)
          ->get('content_pager.changeTitle_' . $content_type)
      );

      // Right column fieldset.
      $form[$content_type]['paging_config']['paging_right'] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#attributes' => array('class' => array('paging-right')),
        '#states' => array(
          'visible' => array(// action to take.
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
          ),
        ),
      );

      $form[$content_type]['paging_config']['paging_right']['paging_automatic_method_' . $content_type] = array(
        '#type' => 'radios',
        '#title' => t('Automatic paging method'),
        '#options' => array(
//          'disabled' => t('Disabled'),
          'chars' => t('Limit by characters <small>(recommended)</small>'),
          'words' => t('Limit by words')
        ),
        '#description' => t('Method for automatic paging (ignored where paging
            separator string is used).'),
        '#attributes' => array('class' => array('paging-method')),
        '#states' => array(
          'visible' => array(// action to take.
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
          ),
        ),
        '#default_value' => \Drupal::config('content_pager.automaticMethod_' . $content_type)
          ->get('content_pager.automaticMethod_' . $content_type)
      );

      $a = \Drupal::config('content_pager.automaticMethodChars_' . $content_type)
        ->get('content_pager.automaticMethodChars_' . $content_type);

//      $char_len_options = array(-1 => 750) + range(500, 7500, 500);
      $char_len_options = array(
        '500' => 500,
        '1000' => 1000,
        '1500' => 1500,
        '2000' => 2000,
        '2500' => 2500,
        '3000' => 3000,
        '3500' => 3500,
        '4000' => 4000,
        '4500' => 4500,
        '5000' => 5000
      );
     asort($char_len_options);

        $word_len = array(
            '100' => 100,
            '150' => 150,
            '200' => 200,
            '250' => 250,
            '300' => 300,
            '350' => 350,
            '400' => 400,
            '450' => 450,
            '500' => 500,
            '550' => 550,
            '600' => 600,
            '650' => 650,
            '700' => 700,
            '750' => 750,
            '800' => 800,
            '850' => 850,
            '900' => 900,
            '950' => 950,
            '1000' => 1000,
        );
        asort($word_len);
        //      $word_len = range(100, 1000, 50);

      // Automatic paging method. Select list to choose the number of characters per page.
      $form[$content_type]['paging_config']['paging_right']['paging_automatic_chars_' . $content_type] = array(
        '#type' => 'select',
        '#title' => t('Length of each page'),
        '#options' => $char_len_options,
        '#field_suffix' => t('characters'),
        '#description' => '<br />' . t('Number of characters to display per page.'),
        '#prefix' => '<div class="container-inline paging-chars paging-chars-' . $content_type . '">',
        '#suffix' => '</div>',
        '#states' => array(
          'visible' => array(
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
            ':input[name="paging_automatic_method_' . $content_type . '"]' => array('value' => 'chars'),
          ),
        ),
        '#default_value' => $a
      );

      // Automatic paging method. Text box to choose orphan size.
      $form[$content_type]['paging_config']['paging_right']['paging_automatic_chars_orphan_' . $content_type] = array(
        '#type' => 'textfield',
        '#title' => t('Length of orphans'),
        '#size' => 6,
        '#field_suffix' => t('characters'),
        '#description' => '<br />' . t('Number of characters to consider as an orphan.'),
        '#prefix' => '<div class="container-inline paging-chars-orphan paging-chars-orphan-' . $content_type . '">',
        '#suffix' => '</div>',
        '#states' => array(
          'visible' => array(
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
            ':input[name="paging_automatic_method_' . $content_type . '"]' => array('value' => 'chars'),
          ),
        ),
        '#default_value' => \Drupal::config('content_pager.automaticMethodCharsOrphan_' . $content_type)
          ->get('content_pager.automaticMethodCharsOrphan_' . $content_type)
      );

      // Automatic paging method. Select list to choose the number of words per page.
      $form[$content_type]['paging_config']['paging_right']['paging_automatic_words_' . $content_type] = array(
        '#type' => 'select',
        '#title' => t('Length of each page'),
        '#options' => $word_len,
        '#field_suffix' => t('words'),
        '#description' => '<br />' . t('Number of words to display per page.'),
        '#prefix' => '<div class="container-inline paging-words paging-words-' . $content_type . '">',
        '#suffix' => '</div>',
        '#states' => array(
          'visible' => array(
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
            ':input[name="paging_automatic_method_' . $content_type . '"]' => array('value' => 'words'),
          ),
        ),
        '#default_value' => \Drupal::config('content_pager.automaticMethodWords_' . $content_type)
          ->get('content_pager.automaticMethodWords_' . $content_type)
      );

      // Automatic paging method. Text box to set orphan page size.
      $form[$content_type]['paging_config']['paging_right']['paging_automatic_words_orphan_' . $content_type] = array(
        '#type' => 'textfield',
        '#title' => t('Length of orphans'),
        '#size' => 6,
        '#field_suffix' => t('words'),

        '#description' => '<br />' . t('Number of wordss to consider as an orphan.'),
        //  '#default_value' => variable_get('paging_automatic_words_orphan_' . $content_type, 200),
        '#prefix' => '<div class="container-inline paging-words-orphan paging-words-orphan-' . $content_type . '">',
        '#suffix' => '</div>',
        '#states' => array(
          'visible' => array(// action to take.
            ':input[name="paging_enabled_' . $content_type . '"]' => array('checked' => TRUE),
            ':input[name="paging_automatic_method_' . $content_type . '"]' => array('value' => 'words'),
          ),
        ),
        '#default_value' => \Drupal::config('content_pager.automaticMethodWordsOrphan_' . $content_type)
          ->get('content_pager.automaticMethodWordsOrphan_' . $content_type)
      );
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save Configuration'),

    );

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $contentTypes = \Drupal::service('entity.manager')
      ->getStorage('node_type')
      ->loadMultiple();
    foreach ($contentTypes as $type) {
      $typeId = $type->id();

     /* $pagingSeparator = \Drupal::service('config.factory')
        ->getEditable('content_pager.separator')
        ->set('content_pager.separator', $form_state->getValue('paging_separator'))
        ->save();*/

      $pagingPagerCount = \Drupal::service('config.factory')
        ->getEditable('content_pager.pagerCount')
        ->set('content_pager.pagerCount', $form_state->getValue('paging_pager_count'))
        ->save();

      //Save pager enable settings
      $pagingSettings = \Drupal::service('config.factory')
        ->getEditable('content_pager.enabled_' . $typeId)
        ->set('content_pager.enabled_' . $typeId, $form_state->getValue('paging_enabled_' . $typeId))
        ->save();

      $pagingSettings = \Drupal::service('config.factory')
        ->getEditable('content_pager.field_' . $typeId)
        ->set('content_pager.field_' . $typeId, $form_state->getValue('paging_field_' . $typeId))
        ->save();

      $pagingReadMore = \Drupal::service('config.factory')
        ->getEditable('content_pager.readMore_' . $typeId)
        ->set('content_pager.readMore_' . $typeId, $form_state->getValue('paging_read_more_enabled_' . $typeId))
        ->save();

      $pagingChangeTitle = \Drupal::service('config.factory')
        ->getEditable('content_pager.changeTitle_' . $typeId)
        ->set('content_pager.changeTitle_' . $typeId, $form_state->getValue('paging_name_title_' . $typeId))
        ->save();

      $pagingAutoMethod = \Drupal::service('config.factory')
        ->getEditable('content_pager.automaticMethod_' . $typeId)
        ->set('content_pager.automaticMethod_' . $typeId, $form_state->getValue('paging_automatic_method_' . $typeId))
        ->save();

//Getting the value of the index for character count

      $char_len_options = array(
        '500' => 500,
        '1000' => 1000,
        '1500' => 1500,
        '2000' => 2000,
        '2500' => 2500,
        '3000' => 3000,
        '3500' => 3500,
        '4000' => 4000,
        '4500' => 4500,
        '5000' => 5000
      );
      $val_arr = array();
      $paging_automatic_chars = $form_state->getValue('paging_automatic_chars_' . $typeId);
      foreach ($char_len_options as $key => $value) {
        $val_arr[$key] = $value;
      }

        //getting the value of the index for word
//        $word_len = range(100, 1000, 50);
        $word_len = array(
            '100' => 100,
            '150' => 150,
            '200' => 200,
            '250' => 250,
            '300' => 300,
            '350' => 350,
            '400' => 400,
            '450' => 450,
            '500' => 500,
            '550' => 550,
            '600' => 600,
            '650' => 650,
            '700' => 700,
            '750' => 750,
            '800' => 800,
            '850' => 850,
            '900' => 900,
            '950' => 950,
            '1000' => 1000,
        );
        $word_arr = array();
        $paging_automatic_word = $form_state->getValue('paging_automatic_words_' . $typeId);
        foreach ($word_len as $key => $value) {
            $word_arr[$key] = $value;
        }

        if ($form_state->getValue('paging_automatic_method_' . $typeId) != 'disabled') {
        if ($form_state->getValue('paging_automatic_method_' . $typeId) == 'chars') {
          // Removing word settings
          $pagingWords = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodWords_' . $typeId)
            ->set('content_pager.automaticMethodWords_' . $typeId, '')
            ->save();

          $pagingWordsOrphan = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodWordsOrphan_' . $typeId)
            ->set('content_pager.automaticMethodWordsOrphan_' . $typeId, '')
            ->save();

          //Setting Paging by Characters
          $pagingChars = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodChars_' . $typeId)
            ->set('content_pager.automaticMethodChars_' . $typeId, $val_arr[$paging_automatic_chars])
            ->save();

          $pagingCharsOrphan = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodCharsOrphan_' . $typeId)
            ->set('content_pager.automaticMethodCharsOrphan_' . $typeId, $form_state->getValue('paging_automatic_chars_orphan_' . $typeId))
            ->save();
        }
        else {
          //Removing char settings
          $pagingChars = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodChars_' . $typeId)
            ->set('content_pager.automaticMethodChars_' . $typeId, '')
            ->save();

          $pagingCharsOrphan = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodCharsOrphan_' . $typeId)
            ->set('content_pager.automaticMethodCharsOrphan_' . $typeId, '')
            ->save();

          //Setting Paging by Words
          $pagingWords = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodWords_' . $typeId)
            ->set('content_pager.automaticMethodWords_' . $typeId,  $word_arr[$paging_automatic_word])
            ->save();

          $pagingWordsOrphan = \Drupal::service('config.factory')
            ->getEditable('content_pager.automaticMethodWordsOrphan_' . $typeId)
            ->set('content_pager.automaticMethodWordsOrphan_' . $typeId, $form_state->getValue('paging_automatic_words_orphan_' . $typeId))
            ->save();
        }
      }
    }
    drupal_set_message(t('The configuration options have been saved. '));
  }
}